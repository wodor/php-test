
The solution consists of two parts. Simple React single-page app and an 
API that provides search functionality and provides a cache for the 
upstream pokeapi.co that is rate limited. 


Run the backend 
---------------

```
cd api 
composer install
docker-compose up -d --build
```

After a couple of minutes, needed for images download and build as well 
as indexing data, backend should become available, URL like 
http://localhost:8080/?query=pika should return 
some search results.

The logs can be checked with `docker-compose logs -f` 
In case of ElasticSearch taking too much time to get up for the first 
time, it may be required to run docker-compose build up again

```
docker-compose stop 
docker-compose up -d --build
``` 

Run the frontend
----------------

```
cd frontend
npm install
npm start
```

Thoughts
--------

In this solution I've emphasized the business value of search, I imagine
that many people don't remember how to spell the exactly especially if 
they only heard it, so I wanted to provide some kind of fuzzy search. 
I also wanted to demonstrate how I would do it efficiently at scale.

I also made sure that whole setup - frontend, backend, ElasticSearch, 
creation of index and indexing is automated for better developer experience. 
I believe that it is important to keep every project easy to start to 
work with, as this eliminates the risk for this project to become a
no man's land.

I haven't put much effort in the frontend application, 
I just came up with autocomplete field that does the trick when the 
name of pokemon is clicked. 
Please forgive me the looks of it, I lost faith in my CSS skills long time ago.  

This task required a very small amount of PHP coding, I've used it only 
for the indexing script and to expose the search endpoint, this could 
be written in two independent transaction scripts, but I've refactored 
it to more professional OOP approach, to make the intentions clearer for 
the reader, at the cost of almost doubling the number of lines of code.

Code is organised in two contexts - Indexing and Searching as I expect 
that in future these would evolve into two different subsystems run in 
different contexts. 

#### Why no tests?

I believe that writing unit tests/specs makes more sense when a bigger 
application is designed, layers and interfaces between them are defined. 
This time I came with PoC approach where I just needed to test the 
idea of indexing and then making the correct query that returns 
satisfying results, there were no other complex parts of code that 
needed heavy coverage. 

In a world with unlimited time, I would retrofit a suite that tests all 
the boilerplate. For example, if exceptions are thrown correctly and 
if the name param is passed to the query.
More reasonable usage of time would be to create a Behat suite
that automates the indexing and querying, this would help to avoid 
regression if more functionality is added. 
I will be happy to show pieces of code made with this approach upon request.
I haven't used any framework this time (though I used some Symfony 
components), but if I was supposed to create just one more controller I 
would start the work with 
`composer create-project symfony/skeleton pokemon-search-api`. 
The code I wrote is portable to any framework, it depends only on 
what it has to depend on - PSR's LoggerInterface and ElasticSearch lib.

#### Why ElasticSearch ?
I thought that just matching the beginning of the string would be easy, 
but not sufficient in this case, 
also it would be great to have some fuzzy matching and have the results 
sorted by how similar they are to the search string. 
Approaches based on substring search can be slow when anything else 
then beginning of the field is considered, 
so I decided to use a proper search engine for it.

It is using edge n-gram approach. It means that during indexing it 
creates all substrings of a word and stores it in the index. 
This can result in a bigger index and make the indexing process slower, 
but we can afford it at this time.
 

#### How is the API cached?
I thought it is better to follow a DevOps path to solve this problem, 
I could create an endpoint that would cache calls to API with the help 
of Redis, but would it behave well at scale? What if 1000s of requests 
dogpiled at the same time? 
I delegated this problem to Nginx's cache proxy which is way more 
reliable than any other solution I could come up with here.
Everything is configured in api/docker/etc/nginx 

#### Dev setup
I had to spend some time to solve the problem of automating indexing 
from Nginx container _after_ the ElasticSearch becomes available. 
I found a simple wait-for-it bash script that helped me to achieve it.
The reindexing approach I propose here is not perfect, it would result 
in the index being unavailable after deployment when it is indexed.
The first step would be to make index rebuild conditional, then it 
would be best to rotate the index in a blue-green manner when the 
index specification changes. 
This would involve automating the build of the new index under a new 
name and deploying the application with this new name as parameter and 
removing the old index after the deployment is confirmed to succeed.
