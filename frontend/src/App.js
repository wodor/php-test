import React from 'react';
import './App.css';
import Autocomplete from 'react-autocomplete';
import Pokemon from './components/Pokemon';
import {getPokemon, getPokemons} from "./components/utils";
import styled from 'styled-components';

const OuterBox = styled.div`
width: 100vw;
height: 100vh;
display: flex;
justify-content: center;
align-items: center;
background: #aaa;
`;
const MainContainer = styled.div`
flex: 0 0 50%;
display: flex;

flex-flow: row nowrap;
`;
const SearchBar = styled.div`
flex: 1;
`;
const PokemonDisplay = styled.div`
flex-flow: column nowrap;
flex: 1;
`;
const Hero = styled.div`
flex: 1;
`;


function App() {

    const [pokemons, setPokemons] = React.useState([]);
    const [value, setValue] = React.useState();
    const [pokemonData, setPokemonData] = React.useState();



    return (
        <OuterBox>
            <MainContainer>
                <Hero>
                    <h1>Pokédex</h1>
                </Hero>
                <SearchBar>
                    <label>Pokemon Name</label>
                    <Autocomplete
                        inputProps={{id: 'states-autocomplete'}}
                        wrapperStyle={{position: 'relative', display: 'inline-block'}}
                        value={value}
                        items={pokemons}
                        getItemValue={(item) => item.name}
                        onSelect={(value, item) => {
                            setValue(item.name)
                            getPokemon(item,setPokemonData)
                        }}
                        onChange={(event, value) => {
                            setValue(value)
                            getPokemons(value, (items) => {
                                setPokemons(items)
                            })
                        }}
                        renderMenu={children => (
                            <div className="menu">
                                {children}
                            </div>
                        )}
                        renderItem={(item, isHighlighted) => (
                            <div
                                className={`item ${isHighlighted ? 'item-highlighted' : ''}`}
                                key={item.key}
                            >{item.name}</div>
                        )}
                    />
                </SearchBar>
                <PokemonDisplay>
                    {pokemonData && <Pokemon pokemon={pokemonData}/>}
                </PokemonDisplay>
            </MainContainer>
        </OuterBox>
    );
}

export default App;

