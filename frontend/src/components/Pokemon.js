import React from "react";
import styled from 'styled-components';

const H3 = styled.h3`
text-transform: capitalize;
`;
const PokeList = styled.ul`
list-style-type: none;
li{
font-weight: bold;
}
`;
function Pokemon({pokemon}) {
    return (
        <>

            <H3>{pokemon.name}</H3>
            <img alt="" src={pokemon.sprites.front_default}/>
            <div>Height: <b>{pokemon.height}</b></div>
            <div>Weight: <b>{pokemon.weight}</b></div>
            <div>Species: <b>{pokemon.species.name}</b></div>
            <div>Abilities: <PokeList>{pokemon.abilities.map(
                (ab) => {return <li key={ab.ability.url}>{ab.ability.name}</li>}
            )}</PokeList></div>

        </>
    )
}

export default Pokemon;
