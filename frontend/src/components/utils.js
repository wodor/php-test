export function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

var backendUrl = 'http://localhost:8080/'

export function getPokemons(value, cb) {
    fetch(backendUrl + '?query=' + value)
        .then(handleErrors)
        .then((res) => res.json())
        .then((data) => {
            cb(data.results.map((v) => ({name: v.name, key: v.url})))
        }).catch((error) => {
        cb([]);
        console.log(error)
    })
}

export function getPokemon(item, setPokemonData) {
    fetch(replaceHost(item.key))
        .then(handleErrors)
        .then((res) => res.json())
        .then((data) => setPokemonData(data))
}

function replaceHost(url) {
    return url.replace(/^.*\/\/[^/]+/, backendUrl)
}
