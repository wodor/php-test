<?php

use Elasticsearch\Common\Exceptions\ElasticsearchException;
use Pokemon\Search\PokemonElasticSearchRepository;

require '../vendor/autoload.php';
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
if (!isset($_GET['query'])) {
    exit ('[]');
}

[
    'esClient' => $esClient,
    'logger' => $logger,
    'config.index.name' => $indexName,
] = require __DIR__ . '/../container.php';

$repo = new PokemonElasticSearchRepository($esClient, $indexName);

try {
    $results = $repo->findByFuzzyName($_GET['query']);
} catch (ElasticsearchException $e) {
    exit(json_encode(['error' => $e->getMessage()]));
} catch (Throwable $e) {
    exit(json_encode(['error' => 'Something went wrong']));
}

echo json_encode(['results' => $results]);

