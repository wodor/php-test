<?php

use Elasticsearch\ClientBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

require __DIR__ . '/vendor/autoload.php';

$apiUrl = 'https://pokeapi.co/api/v2/';
$esHosts = ['http://localhost:9200', 'http://elasticsearch:9200'];

$container['esClient'] = ClientBuilder::create()->setHosts($esHosts)->build();
$container['logger'] = new Logger('indexer', [new StreamHandler('php://stdout', Logger::DEBUG)]);

$container['config.index.name'] = 'pokemon';

return $container;
