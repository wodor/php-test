<?php

declare(strict_types=1);

namespace Pokemon\Index;

use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\ElasticsearchException;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Psr\Log\LoggerInterface;

class ElasticSearchIndexer
{
    private Client $esClient;
    private LoggerInterface $logger;
    private string $index;

    public function __construct(Client $esClient, string $index, LoggerInterface $logger)
    {
        $this->esClient = $esClient;
        $this->index = $index;
        $this->logger = $logger;
    }

    public function deleteIndex(): void
    {
        try {
            $deleteParams = [
                'index' => $this->index,
            ];
            $this->esClient->indices()->delete($deleteParams);

        } catch (Missing404Exception $e) {
            $this->logger->debug("No index to remove, usually this is ok");
        } catch (ElasticsearchException $e) {
            $this->logger->error(sprintf("Failed to remove index %s", $e->getMessage()));
            throw new IndexingException('Failed to index',500, $e);
        }
    }

    public function createIndex(): void
    {
        try {
            $params = [
                'index' => $this->index,
                'body' => json_decode(file_get_contents(__DIR__ . '/edge_ngram.index.json'), true),
            ];

            $this->esClient->indices()->create($params);
        } catch (ElasticsearchException $e) {
            $this->logger->error(sprintf("Failed to index item %s", $e->getMessage()));
            throw new IndexingException('Failed to index',500, $e);
        }
    }

    public function indexData(array $data): void
    {
        try {
            foreach ($data as $result) {
                $params = [
                    'index' => $this->index,
                    'id' => md5($result['url']),
                    'body' => $result,
                ];

                $this->esClient->index($params);
            }
        } catch (ElasticsearchException $e) {
            $this->logger->error(sprintf("Failed to index item %s", $e->getMessage()));
            throw new IndexingException('Failed to index',500, $e);
        }
    }
}
