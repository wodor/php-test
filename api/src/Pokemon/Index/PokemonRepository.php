<?php

declare(strict_types=1);

namespace Pokemon\Index;

interface PokemonRepository
{
    public function findAll(int $limit): array;
}
