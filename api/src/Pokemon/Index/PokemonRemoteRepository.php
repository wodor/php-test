<?php

declare(strict_types=1);

namespace Pokemon\Index;

use Symfony\Component\HttpClient\HttpClient;

final class PokemonRemoteRepository implements PokemonRepository
{
    private string $apiUrl;

    public function __construct(string $apiUrl)
    {
        $this->apiUrl = $apiUrl;
    }

    public function findAll(int $limit): array
    {
        try {
            $client = HttpClient::create();
            $response = $client->request('GET', sprintf("%spokemon/?limit=%d", $this->apiUrl, $limit));
            $statusCode = $response->getStatusCode();
            $data = $response->toArray();

            if (!$statusCode === 200 || !isset($data['results'])) {
                throw new IndexingException('Failed to get pokemon data');
            }

            return $data['results'];
        } catch (\Throwable $e) {
            // TODO: own exception
            throw new IndexingException('Failed to get pokemon data');
        }
    }
}
