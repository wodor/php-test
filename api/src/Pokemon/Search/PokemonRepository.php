<?php

declare(strict_types=1);

namespace Pokemon\Search;

interface PokemonRepository
{
    public function findByFuzzyName(string $name): array;
}
