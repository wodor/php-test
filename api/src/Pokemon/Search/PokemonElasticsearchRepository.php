<?php

declare(strict_types=1);

namespace Pokemon\Search;

use Elasticsearch\Client;

final class PokemonElasticSearchRepository implements PokemonRepository
{
    private Client $esClient;
    /** @var string */
    private string $index;

    public function __construct(Client $esClient, string $index)
    {
        $this->esClient = $esClient;
        $this->index = $index;
    }

    public function findByFuzzyName(string $name): array
    {
        $result = $this->esClient->search(
            [
                'index' => $this->index,
                'body' => [
                    'query' => [
                        'match' => [
                            'name' => [
                                'query' => $name,
                                'operator' => 'and',
                                'fuzziness' => 'AUTO',
                            ],
                        ],
                    ],
                ],
            ]
        );

        $results = [];
        foreach ($result['hits']['hits'] as $hit) {
            $results[] = $hit['_source'];
        }

        return $results;
    }
}
