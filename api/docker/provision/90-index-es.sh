#!/bin/bash
set -e
set +u

echo 'Waiting up to one minute for elasticsearch to come up, retry if it did not manage '
/wait-for-it.sh -h elasticsearch -p 9200 -t 60

php /app/bin/reindex.php
