<?php

use Pokemon\Index\ElasticSearchIndexer;
use Pokemon\Index\PokemonRemoteRepository;

[
    'esClient' => $esClient,
    'logger' => $logger,
    'config.index.name' => $indexName,
] = require __DIR__ . '/../container.php';

$indexer = new ElasticSearchIndexer($esClient, $indexName, $logger);
$pokemonRepo = new PokemonRemoteRepository($apiUrl);

try {
    $pokemonData = $pokemonRepo->findAll(1000);

    $logger->info(sprintf("Indexing pokemon"), ['count' => count($pokemonData)]);

    $indexer->deleteIndex();
    $indexer->createIndex();
    $indexer->indexData($pokemonData);
    $logger->info("Indexing finished");

} catch (Throwable $exception) {
    $logger->error($exception->getMessage());
    exit(1);
}

